#include <cstdio>
#include <cstring>
#include <stdio.h>
#include <string.h>

/*
Aceasta este o structura care sa reprezinte cuvinte
 val - valoarea cuvantului (e.g. abc aaa daca ...)
 grad - gradul cuvantului (1, 10, 9)
*/
struct Cuvant{
  char val[64];
  int grad;
};

using namespace std;

// http://varena.ro/problema/grad

int valoareLitera(char lit){
    // 97 - a
    int val = (int) lit;
    val -= 96;
    return val;
}

int gradCuvant(struct Cuvant cuv){
    char * caracterCurent = cuv.val;
    int grad = 0;
    while(*caracterCurent){
        grad += valoareLitera(*caracterCurent);
        caracterCurent++;
    }
    return grad;
}

void afisareCuvant(struct Cuvant cuv){
    printf("Cuvant: %s, grad %d\n", cuv.val, cuv.grad);
}

int main()
{
    
    // printf("Val: %d\n", valoareLitera('a'));
    FILE * pf = fopen("grad.in", "r");

    Cuvant cuvinteFisier[30];
    int nrCuvinte = 0;
    
    char buffer[1024];
    
    
    fscanf(pf, " %s", buffer);
    // 1. citim cuvintele din fisier si le punem in cuvinteFisier[]
    while (fscanf(pf, " %s", buffer) == 1) {
        strcpy(cuvinteFisier[nrCuvinte].val, buffer);
        nrCuvinte++;
    }
    
    // 2. pentru fiecare cuvant, calculam gradul
    int i;
    for(i=0; i<nrCuvinte; i++){
        int grad = gradCuvant(cuvinteFisier[i]);
        cuvinteFisier[i].grad = grad;
    }
    
    // 3. afisam rezultatele
    for(i=0; i<nrCuvinte; i++){
        afisareCuvant(cuvinteFisier[i]);
    }
    
    // 4. grupare cuvinte dupa grad
    int grade[500];
    
    for(i=0; i<500; i++){
        grade[i] = 0;
    }
    
    for(i=0; i<nrCuvinte; i++){
        grade[cuvinteFisier[i].grad]++;
        // printf("%d\n", cuvinteFisier[i].grad);
    }
    
    int numarGrupe = 0;
    for(i=0; i<100; i++){
        // printf("%d\n", grade[i]);
        if(grade[i] > 0){
            numarGrupe++;
        }
    }
    printf("Numarul de grupe gasit este: %d\n", numarGrupe);
    printf("Numarul de cuvinte este: %d\n", nrCuvinte);
    // ... inchidem fisierul
    fclose(pf);
     
    return 0;
}



